package org.dnyanyog.controller;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.response.GetUserResponse;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class LoginController {

	@Autowired
	UserService userService;
	
	@PostMapping(path = "directory/api/v1/validate" , produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request) {
		

		return userService.login(request);

}
	
	@GetMapping(path="directory/api/v1/user/{userId}")
	public SignUpResponse getUserById(@PathVariable long userId) {
		return userService.getUserById(userId);
	}
	@GetMapping(path="directory/api/v1/users")
	public  GetUserResponse getUser() {
		 return userService.getUser();
	}
}