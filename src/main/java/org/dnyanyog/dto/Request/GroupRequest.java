package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class GroupRequest {
	private String groupName;
	private String groupType;
	private long groupId;

	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

}
