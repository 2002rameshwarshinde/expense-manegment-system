package org.dnyanyog.service;

import org.dnyanyog.dto.request.GroupRequest;
import org.dnyanyog.dto.response.GroupData;
import org.dnyanyog.dto.response.GroupResponse;
import org.dnyanyog.entity.GroupsInformation;
import org.dnyanyog.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class GroupService {
	
	@Autowired
	GroupResponse groupResponse;
	
	@Autowired
	GroupRepository groupRepository;
	@Autowired
	GroupsInformation group;

	public ResponseEntity<GroupResponse> createGroup(GroupRequest request) {
		groupResponse=new GroupResponse();
		groupResponse.setData(new GroupData());
		
		if(null != groupRepository.findByGroupName(request.getGroupName())) {
			groupResponse.setStatus("error");
			groupResponse.setMessage("Group already exit");
			groupResponse.setData(null);
			return ResponseEntity.status(HttpStatus.CONFLICT).body(groupResponse);
		}
		group=new GroupsInformation();
		group.setGroupName(request.getGroupName());
		group.setGroup_Id(request.getGroupId());
		group.setGroupType(request.getGroupType());
		group=groupRepository.save(group);
		groupResponse.setStatus("success");
		groupResponse.setMessage("Group created successfully");
		groupResponse.getData().setGroupId(group.getGroup_Id());
		groupResponse.getData().setGroupName(group.getGroupName());
		groupResponse.getData().setGroupType(group.getGroupType());
		return ResponseEntity.status(HttpStatus.CREATED).body(groupResponse); 
		}
}
