package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table
@Entity
@Component
public class GroupsInformation {
	@Id
	@GeneratedValue
	private long group_Id;
	public long getGroup_Id() {
		return group_Id;
	}
	public void setGroup_Id(long group_Id) {
		this.group_Id = group_Id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	@Column
	private String groupName;
	@Column
	private String groupType;
	
	

}
